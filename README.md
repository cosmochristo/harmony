# Harmony

The Harmony Project's goal is to move the digital world to high-resolution spaces and positionally invariant accuracy by applying origin-centric techniques. 
This approach leads to greater fidelity and scalability.
The sub-goal is to promulgate high-fidelity motion, interaction and calculation, an optimising simulation architecture, code and practices.
All files in this project relate to the research documented here: https://www.researchgate.net/profile/Chris_Thorne3.
The scripts provided here are for the Unity game engine.